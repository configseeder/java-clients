# Java Clients for ConfigSeeder®

Welcome to native Java integration project for *ConfigSeeder®*

## Integration Examples

There are various ways how ConfigSeeder® can be connected to your application:

- Using native Java clients for your application:
  - Spring (see [example Spring integration](examples/spring/README.md))
    ```xml
    <dependency>
      <groupId>com.configseeder.client</groupId>
      <artifactId>configseeder-client-spring</artifactId>
    </dependency>
    ```
  - Eclipse Micro Profile Config (see [example Quarkus integration](examples/quarkus/README.md))
    - Smallrye based
      ```xml
      <dependency>
        <groupId>com.configseeder.client</groupId>
        <artifactId>configseeder-client-smallrye-config</artifactId>
      </dependency>
      ```
    - Eclipse Micro Profile Config based
      ```xml
      <dependency>
        <groupId>com.configseeder.client</groupId>
        <artifactId>configseeder-client-microprofile-config</artifactId>
      </dependency>
      ```
  - Maven (see [example Maven integration](examples/maven/README.md))
    ```xml
    <build>
    <plugins>
      <plugin>
        <groupId>com.configseeder.client.maven</groupId>
        <artifactId>configseeder-client-maven-plugin</artifactId>
        </plugin>
      </plugins>
    </build>
    ```
  - Native Java client (see [example JavaFX integration](examples/javafx/README.md))
    ```xml
    <dependency>
      <groupId>com.configseeder.client</groupId>
      <artifactId>configseeder-client-core</artifactId>
    </dependency>
    ```

## Contributing

Feel free to provide any feedback, pull requests or contact us on https://configseeder.com/contact/?topic=other
