/*
 * Copyright (c) 2022 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client;

import com.configseeder.client.model.ConfigurationEntryRequest;
import com.configseeder.client.model.ConfigurationRequest;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests read of configuration model.
 */
class ConfigSeederClientConfigurationReaderTest {

    @Test
    void shouldReadFromPropertiesFull() {
        // given
        String file = "/configseeder-full.properties";

        // when
        ConfigSeederClientConfiguration configuration = ConfigSeederClientConfiguration.fromProperties(file);

        // then
        verifyFullContent(configuration);
    }

    @Test
    void shouldReadFromYamlFull() {
        // given
        String file = "/configseeder-full.yaml";

        // when
        ConfigSeederClientConfiguration configuration = ConfigSeederClientConfiguration.fromYaml(file);

        // then
        verifyFullContent(configuration);
    }

    @Test
    void shouldReadFromPropertiesMinimal1() {
        // given
        String file = "/configseeder-minimal-1.properties";

        // when
        ConfigSeederClientConfiguration configuration = ConfigSeederClientConfiguration.fromProperties(file);

        // then
        verifyDefaultsAndMinimalContent(configuration);
    }

    @Test
    void shouldReadFromMinimal1() {
        // given
        String file = "/configseeder-minimal-1.yaml";

        // when
        ConfigSeederClientConfiguration configuration = ConfigSeederClientConfiguration.fromYaml(file);

        // then
        verifyDefaultsAndMinimalContent(configuration);
    }

    @Test
    void shouldReadFromMinimal2() {
        // given
        String file = "/configseeder-minimal-2.yaml";

        // when
        ConfigSeederClientConfiguration configuration = ConfigSeederClientConfiguration.fromYaml(file);

        // then
        verifyDefaultsAndMinimalContent(configuration);
    }

    private void verifyDefaultsAndMinimalContent(ConfigSeederClientConfiguration configuration) {
        assertThat(configuration.getInitializationMode()).isEqualTo(ConfigSeederClientConfiguration.DEFAULT_INITIALIZATION_MODE);
        assertThat(configuration.getRefreshMode()).isEqualTo(ConfigSeederClientConfiguration.DEFAULT_REFRESH_MODE);
        assertThat(configuration.getMaxRetries()).isEqualTo(ConfigSeederClientConfiguration.DEFAULT_MAX_RETRIES);
        assertThat(configuration.getRetryWaitTime()).isEqualTo(ConfigSeederClientConfiguration.DEFAULT_RETRY_WAIT_TIME);
        assertThat(configuration.getRefreshCycle()).isEqualTo(ConfigSeederClientConfiguration.DEFAULT_REFRESH_CYCLE);
        assertThat(configuration.getServerUrl()).isEqualTo("http://localhost:8080");
        assertThat(configuration.getApiKey()).isEqualTo("any-api-key");

        ConfigurationRequest configurationRequest = configuration.getRequestConfiguration();
        assertThat(configurationRequest.getEnvironmentKey()).isEqualTo("PROD");
        assertThat(configurationRequest.getTenantKey()).isEqualTo(ConfigurationRequest.DEFAULT_TENANT_KEY);
        assertThat(configurationRequest.getContext()).isNull();
        assertThat(configurationRequest.getVersion()).isNull();
        assertThat(configurationRequest.getConfigurations().size()).isEqualTo(2);

        ConfigurationEntryRequest group1 = getEntryWithGroupName(configurationRequest, "config-group-1");
        assertThat(group1.getConfigurationGroupKey()).isEqualTo("config-group-1");
        //assertThat(group1.getSelectionMode()).isEqualTo(VersionedConfigurationGroupSelectionMode.LATEST);
        //assertThat(group1.getConfigurationGroupVersionNumber()).isNull();

        ConfigurationEntryRequest group2 = getEntryWithGroupName(configurationRequest, "config-group-2");
        assertThat(group2.getConfigurationGroupKey()).isEqualTo("config-group-2");
        //assertThat(group2.getSelectionMode()).isEqualTo(VersionedConfigurationGroupSelectionMode.LATEST);
        //assertThat(group2.getConfigurationGroupVersionNumber()).isNull();
    }


    private void verifyFullContent(ConfigSeederClientConfiguration configuration) {
        assertThat(configuration.getInitializationMode()).isEqualTo(InitializationMode.LAZY);
        assertThat(configuration.getRefreshMode()).isEqualTo(RefreshMode.LAZY);
        assertThat(configuration.getMaxRetries()).isEqualTo(5);
        assertThat(configuration.getRetryWaitTime()).isEqualTo(234);
        assertThat(configuration.getRefreshCycle()).isEqualTo(12345);
        assertThat(configuration.getServerUrl()).isEqualTo("http://localhost:8080");
        assertThat(configuration.getApiKey()).isEqualTo("any-api-key");

        ConfigurationRequest configurationRequest = configuration.getRequestConfiguration();
        assertThat(configurationRequest.getEnvironmentKey()).isEqualTo("PROD");
        assertThat(configurationRequest.getTenantKey()).isEqualTo("TEN");
        assertThat(configurationRequest.getContext()).isEqualTo("my-context");
        assertThat(configurationRequest.getVersion()).isEqualTo("1.5.0-beta");
        assertThat(configurationRequest.getConfigurations().size()).isEqualTo(3);

        ConfigurationEntryRequest group1 = getEntryWithGroupName(configurationRequest, "config-group-1");
        assertThat(group1.getConfigurationGroupKey()).isEqualTo("config-group-1");
        //assertThat(group1.getSelectionMode()).isEqualTo(VersionedConfigurationGroupSelectionMode.LATEST);
        //assertThat(group1.getConfigurationGroupVersionNumber()).isNull();

        ConfigurationEntryRequest group2 = getEntryWithGroupName(configurationRequest, "config-group-2");
        assertThat(group2.getConfigurationGroupKey()).isEqualTo("config-group-2");
        //assertThat(group2.getSelectionMode()).isEqualTo(VersionedConfigurationGroupSelectionMode.LATEST_RELEASED);
        //assertThat(group2.getConfigurationGroupVersionNumber()).isNull();

        ConfigurationEntryRequest group3 = getEntryWithGroupName(configurationRequest, "config-group-3");
        assertThat(group3.getConfigurationGroupKey()).isEqualTo("config-group-3");
        //assertThat(group3.getSelectionMode()).isEqualTo(VersionedConfigurationGroupSelectionMode.VERSION_NUMBER);
        //assertThat(group3.getConfigurationGroupVersionNumber()).isEqualTo(5);
    }

    private ConfigurationEntryRequest getEntryWithGroupName(ConfigurationRequest configurationRequest, String groupKey) {
        return configurationRequest.getConfigurations().stream()
                .filter(r -> r.getConfigurationGroupKey().equals(groupKey))
                .findFirst().orElse(null);
    }

}