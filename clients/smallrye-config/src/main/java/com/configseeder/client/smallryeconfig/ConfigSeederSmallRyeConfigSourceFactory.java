/*
 * Copyright (c) 2022 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package com.configseeder.client.smallryeconfig;

import static java.util.Collections.emptyList;

import com.configseeder.client.ConfigSeederClient;
import com.configseeder.client.ConfigSeederClientConfiguration;
import com.configseeder.client.model.Identification;
import io.smallrye.config.ConfigSourceContext;
import io.smallrye.config.ConfigSourceFactory;
import io.smallrye.config.ConfigValue;
import java.util.Collections;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.logging.Level;
import lombok.extern.java.Log;
import org.eclipse.microprofile.config.spi.ConfigSource;

@Log
public class ConfigSeederSmallRyeConfigSourceFactory implements ConfigSourceFactory {

    @Override
    public Iterable<ConfigSource> getConfigSources(final ConfigSourceContext context) {
        try {
            ConfigSeederClientConfigurationProvider clientConfigurationProvider = new ConfigSeederClientConfigurationProvider();
            ConfigSeederClientConfiguration clientConfiguration = clientConfigurationProvider.getClientConfiguration(context);
            clientConfiguration.validate();

            if (clientConfiguration.isDisabled()) {
                 return Collections.emptyList();
            }

            ConfigSeederClient configSeederClient = new ConfigSeederClient(clientConfiguration, Identification.create("SmallRyeConfig"));
            ConfigValue priorityConfig = context.getValue("configseeder.client.config-source.priority");
            Integer priority = Optional.ofNullable(priorityConfig).map(ConfigValue::getValue).map(Integer::valueOf).orElse(200);
            return Collections.singletonList(new ConfigSeederSmallRyeConfigSource(configSeederClient, priority));
        } catch (Exception ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
            return emptyList();
        }
    }

    @Override
    public OptionalInt getPriority() {
        return OptionalInt.of(150);
    }

}
