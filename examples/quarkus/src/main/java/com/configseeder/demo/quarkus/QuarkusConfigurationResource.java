/*
 * Copyright (c) 2023 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.demo.quarkus;

import static java.util.stream.Collectors.toList;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.config.Config;

@Path("/")
@Getter
@Setter
public class QuarkusConfigurationResource {


    @Inject
    MailSettingsConfig mailSettingsConfig;

    @Inject
    MailBean mailBean;

    @Inject
    Config config;

    /**
     * All existing configuration keys.
     *
     * @return list of all existing configuration keys
     */
    @GET
    @Path("all-keys")
    @Produces("application/json")
    public Response allKeys() {
        final Iterable<String> propertyNames = config.getPropertyNames();
        final List<String> keys = StreamSupport.stream(propertyNames.spliterator(), false).collect(toList());
        return Response.ok(keys).build();
    }

    /**
     * Mail Settings based on properties mail.from and mail.to.
     *
     * @return mail settings as JSON
     */
    @GET
    @Path("mail-settings")
    @Produces("application/json")
    public Response getConfigurations() {
        return Response.ok(new MailSettings(mailBean.getMailHost(), mailSettingsConfig.from(), mailSettingsConfig.to().orElse("null"))).build();
    }

    /**
     * Exposes all configurations with this key.
     * <p>
     * WARNING: NEVER DO THIS IN PRODUCTION! YOU MAY LEAK SENSITIVE CONFIGURATIONS.
     * </p>
     *
     * @param key configuration key
     * @return response
     */
    @GET
    @Path("specific/{key}")
    @Produces("text/plain")
    public Response specificKey(@PathParam("key") String key) {
        final Optional<String> value = config.getOptionalValue(key, String.class);
        if (value.isPresent()) {
            return Response.ok(value.get()).build();
        } else {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

}
