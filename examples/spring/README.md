# ConfigSeeder Spring Boot Demo

## Start

* With your favorite **IDE**:
  * Main Class: `com.configseeder.demo.spring.SpringDemoApplication`
* Server runs on Port 8082.

## Democase

This demo shows you how to easily integrate ConfigSeeder into your application.

1. Include dependency to configseeder in your `pom.xml`:
  
   ```xml
   <dependency>
     <groupId>com.configseeder.client</groupId>
     <artifactId>configseeder-client-spring</artifactId>
   </dependency>
   ```

2. Configure configseeder client with a config file in `/src/main/resources/configseeder.yaml`.
   Relevant configurations: `serverUrl`, `apiKey`. `configurationGroupKeys`, `environmentKey`.
   
   1. Use a user defined on https://configseeder.com/demo/ and login to our demo instance on https://demo.configseeder.com/.
   1. Create an own configuration group
   1. Create an API key
   1. Update configuration and set `tenantKey` to `demo`.
   1. Update configuration values `apiKey`, `configurationGroupKeys` (and if you wish also `environmentKey`).
   

3. Use standard Spring Configuration Injection mechanism:

   ```java
   @Autowired
   private Environment environment;

   @Value("${mail.host:unknown}")
   private String staticMailHostValue;
   
   private void someMethod() {
       // get mail host value. will not change at runtime unless spring context is realoaded
       log.info("Mail Host: {}", staticMailHostValue);
       
       // get the last value (may be changed at runtime with on config seeder)
       log.info("Mail Destination: {}", environment.getProperty("mail.destination"));
   }
   ``` 

## REST Endpoints

(see `DemoConfigurationController`)

- `GET /` => Shows Welcome Page
- `GET /all-keys/` => Shows all Keys
- `GET /mail.host` => Shows contents of key `mail.host` (not changeable at runtime)
- `GET /specific/<key>/` => Shows the configuration value for the given key. (WARNING: Never do such thing in production!)

